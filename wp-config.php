<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'w_ie' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ';cOGy42cfV7V}9h@f*R}v#W6e*o_2b=&HNyYwo_vExVl4#M/WxmwAGLJ{2W=aa`e' );
define( 'SECURE_AUTH_KEY',  '7}:DxhCG G}@&B!p%<dW(#-Kq&T6GHXQrHICP#P;8HE^S;;6>|v48VDWA!kOv.2[' );
define( 'LOGGED_IN_KEY',    's~?r(Kc2:MFQ?BjF3nuQrGX)@Z[,LRa+2`+RLZ]IP: @($JLI<~~1kM-DD@y*QKs' );
define( 'NONCE_KEY',        ' QDO<*++ksn[m v]F9, b{CCGu@Zn$p,ay_M3vah6Xcu!*3t?z,&W0dPIr[/wh3b' );
define( 'AUTH_SALT',        '$2P(LdM8SP(xs%J,Zi@xH,czBF`k.a#$)U&^>3^!<}<I^tC_cZp>lTrhv(qQ)I(O' );
define( 'SECURE_AUTH_SALT', 'J;>ACPn53v3iJ]u)-A w%Nl#`C#yVhGaAqJza&?6nl<WGDmMxc[}8ku/c.<9F.!v' );
define( 'LOGGED_IN_SALT',   '($fvFmlA4`dE=@2x_Xx?:cw`+~5x(O>/;u{SU3QY=Dj)aHglj]|U1Do+(N=E a?#' );
define( 'NONCE_SALT',       '>N ,RDN-+)}E0hwIGK/vr>HmLto)LE:e#5&#_p.NDc,LAJ{XPg8I{=v;l&%l!_4g' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
